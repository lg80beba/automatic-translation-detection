import json
import numpy as np
import random
from keras.preprocessing.sequence import pad_sequences
from keras.models import Model, Sequential
from keras.layers import Embedding, LSTM, Dropout, TimeDistributed, Activation, Dense, Input
from keras.utils import to_categorical

sample_size = 50_000

with open('corpus_100k.json', 'r') as file:
    corpus = json.load(file)
    corpus = random.sample(list(filter(None, [x if ('pos' in x['target_machine'].keys()) and ('pos' in x['target_human'].keys()) else None for x in corpus])), sample_size)

def load_data(corpus, k = 0.9):
    feature = 'pos'

    # Generate tagset to replace tags with numerical values
    tagset = []
    for data in corpus:
        tagset.extend(data['target_machine']['pos'])
        tagset.extend(data['target_human']['pos'])
    tagset = list(enumerate(set(tagset), start=1))
    vocabulary = dict([list(reversed(x)) for x in tagset])
    
    # Translate sequences to numerical based on vocabulary
    sequences = []
    
    for data in corpus:
        sequences.append(([vocabulary[x] for x in data['target_machine'][feature]], 0)) # Machine class = 0
        sequences.append(([vocabulary[x] for x in data['target_human'][feature]], 1)) # Human class = 1

    transposed = list(zip(*sequences))
    
    # Padding to same sequence length
    padded = list(zip(pad_sequences(transposed[0]), transposed[1]))
    
    # build the complete vocabulary, then convert text data to list of integers
    cutoff = int(len(padded)*k)
    train_data = padded[0:cutoff]
    test_data = padded[cutoff:]

    vocabulary = len(tagset)+1 # +1 since 0-category used to fill sequences must be added
    reversed_dictionary = dict(tagset)
    
    X_train = np.array(list(zip(*train_data))[0])
    Y_train = np.array(list(zip(*train_data))[1])

    X_test = np.array(list(zip(*test_data))[0])
    Y_test = np.array(list(zip(*test_data))[1])
    
    return (X_train, Y_train), (X_test, Y_test), vocabulary

(X_train, Y_train), (X_test, Y_test), vocabulary = load_data(corpus, k = 0.9)

# Convert to categorial
Y_test = to_categorical(Y_test)
Y_train = to_categorical(Y_train)

# create the model
model = Sequential()
model.add(Embedding(vocabulary, 32, input_length=X_train.shape[1]))
model.add(LSTM(32, return_sequences=True)) 
model.add(Dropout(0.5))
model.add(LSTM(32, return_sequences=True))
model.add(Dropout(0.5))
model.add(LSTM(32))
model.add(Dense(2, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
print(model.summary())
model.fit(X_train, Y_train, epochs=4, batch_size=64)

# Final evaluation of the model
scores = model.evaluate(X_test, Y_test, verbose=0)
print("Accuracy: %.2f%%" % (scores[1]*100))

