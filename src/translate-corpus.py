import argparse
import re
from time import sleep
from timeit import default_timer as timer
from googletrans import Translator

class TranslateWithGoogle:
    def __init__(self, args):
        self.translator = Translator(service_urls=[
            'translate.google.com',
            'translate.google.de',
            'translate.google.fr',
            'translate.google.it',
            'translate.google.cz',
            'translate.google.pl',
            'translate.google.sk',
            'translate.google.be',
            'translate.google.at',
            'translate.google.bg',
            'translate.google.ch',
            'translate.google.dk',
            'translate.google.gr',
            'translate.google.hu',
            'translate.google.hr',
            'translate.google.ie',
            'translate.google.lu',
            'translate.google.lv',
            'translate.google.me',
            'translate.google.no',
            'translate.google.ro',
            'translate.google.rs',
            'translate.google.si',
            'translate.google.ee',
            'translate.google.fi',
            'translate.google.lt',
            'translate.google.li',
            'translate.google.se',
            'translate.google.es',
            'translate.google.co.za',
            'translate.google.ru',
            'translate.google.co.uk',
        ])

        self.file_path_original = args.file
        self.file_path_target = args.target
        self.text_buffer = ''
        self.last_visit = timer()
        self.delta_time = 0

    def translate(self, text = '', flush=False):
        if flush or len(self.text_buffer) + len(text) > 5000:
            translated_buffer = self.translateBuffer()
            self.text_buffer = text
            return translated_buffer
        else:
            self.text_buffer += text
            return ''

    def translateBuffer(self):
        while True:
            try:
                translation = self.translator.translate(self.text_buffer, src='en', dest='de').text
                trailing_nl = re.search('([\n]*)$', self.text_buffer)
                leading_nl = re.search('^([\n]*)', self.text_buffer)
                if trailing_nl:
                    translation += trailing_nl.group(1)
                if leading_nl:
                    translation = leading_nl.group(1) + translation

                return translation
            except Exception:
                sleep(1)

    def translateLineByLine(self):
        try:
            with open(self.file_path_target) as f:
                already_translated = len(f.readlines())
        except FileNotFoundError:
            already_translated = 0

        with open(self.file_path_target, 'a+') as t:
            with open(self.file_path_original) as s:
                for i, line in enumerate(s):
                    if i >= already_translated:
                        t.write(self.translate(line))
                        self.printProgressBar(i + 1, 1920209, prefix='Progress:', suffix='Complete', length=50, refresh_interval=100)
                t.write(self.translate(flush=True))

    def printProgressBar(self, iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', refresh_interval=1):
        """
        Call in a loop to create terminal progress bar
        @params:
            iteration   - Required  : current iteration (Int)
            total       - Required  : total iterations (Int)
            prefix      - Optional  : prefix string (Str)
            suffix      - Optional  : suffix string (Str)
            decimals    - Optional  : positive number of decimals in percent complete (Int)
            length      - Optional  : character length of bar (Int)
            fill        - Optional  : bar fill character (Str)
        """
        if iteration%refresh_interval == 0:
            self.delta_time = (timer() - self.last_visit) * 0.1 + self.delta_time * 0.9
            remaining = (self.delta_time / refresh_interval) * (total - iteration)
            hours, remainder = divmod(remaining, 3600)
            minutes, seconds = divmod(remainder, 60)
            suffix += ' {:02}:{:02}:{:02} left'.format(int(hours), int(minutes), int(seconds))
            self.last_visit = timer()

            percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
            filledLength = int(length * iteration // total)
            bar = fill * filledLength + '-' * (length - filledLength)
            print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
            # Print New Line on Complete
            if iteration == total:
                print()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Translate the corpus')
    parser.add_argument('file', default='./Corpus/europarl-v7.de-en.en', help='File to translate.')
    parser.add_argument('--target', type=str, default='./translation.txt', help='Target file.')
    args = parser.parse_args()

    twg = TranslateWithGoogle(args)

    twg.translateLineByLine()
