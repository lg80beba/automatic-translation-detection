import argparse
import datetime

import h5py
import numpy as np
import progressbar
from keras import Input, Model
from keras.callbacks import TensorBoard, EarlyStopping
from keras.layers import LSTM, Dense, concatenate, Dropout
from tensorboard.plugins.pr_curve import summary as pr_summary

np.random.seed(42)

LABEL_HUMAN = 0
LABEL_MACHINE = 1

class PRTensorBoard(TensorBoard):
    def __init__(self, *args, **kwargs):
        # One extra argument to indicate whether or not to use the PR curve summary.
        self.pr_curve = kwargs.pop('pr_curve', True)
        super(PRTensorBoard, self).__init__(*args, **kwargs)

        global tf
        import tensorflow as tf

    def set_model(self, model):
        super(PRTensorBoard, self).set_model(model)

        if self.pr_curve:
            # Get the prediction and label tensor placeholders.
            predictions = self.model._feed_outputs[0]
            labels = tf.cast(self.model._feed_targets[0], tf.bool)
            # Create the PR summary OP.
            self.pr_summary = pr_summary.op(name='pr_curve',
                                            predictions=predictions,
                                            labels=labels,
                                            display_name='Precision-Recall Curve')

    def on_epoch_end(self, epoch, logs=None):
        super(PRTensorBoard, self).on_epoch_end(epoch, logs)

        if self.pr_curve and self.validation_data:
            # Get the tensors again.
            tensors = self.model._feed_targets + self.model._feed_outputs
            # Predict the output.
            predictions = self.model.predict(self.validation_data[:-2])
            # Build the dictionary mapping the tensor to the data.
            val_data = [self.validation_data[-2], predictions]
            feed_dict = dict(zip(tensors, val_data))
            # Run and add summary.
            result = self.sess.run([self.pr_summary], feed_dict=feed_dict)
            self.writer.add_summary(result[0], epoch)
        self.writer.flush()


class PLSTM:
    def __init__(self, source, targetHuman, tagetMachine, multilingual, dropout, nodes = 16):
        self.nodes = nodes
        self.sourceFile = h5py.File(source, 'r')
        self.targetHumanFile = h5py.File(targetHuman, 'r')
        self.targetMachineFile = h5py.File(tagetMachine, 'r')
        self.multilingual = multilingual
        self.dropout = dropout
        self.word_vector_dim = 300
        self.data_length = len(self.sourceFile)
        self.sentence_length = 20
        self.trainIndices = np.reshape([[(x, LABEL_HUMAN), (x, LABEL_MACHINE)] for x in range(self.data_length)],
                                       (2 * self.data_length, 2))
        self.x_train_a, self.x_train_b, self.y_train = self.getData(self.trainIndices)

    def getSourceTargetHumanPair(self, index):
        return np.asarray(self.sourceFile[str(index)]), np.asarray(self.targetHumanFile[str(index)]), LABEL_HUMAN

    def getSourceTargetMachinePair(self, index):
        return np.asarray(self.sourceFile[str(index)]), np.asarray(self.targetMachineFile[str(index)]), LABEL_MACHINE

    def getData(self, indices):
        bar = progressbar.ProgressBar(max_value=len(indices), prefix="Loading data")

        inputs_a = np.zeros((len(indices), self.sentence_length, self.word_vector_dim))
        inputs_b = np.zeros((len(indices), self.sentence_length, self.word_vector_dim))
        labels = np.zeros((len(indices), 2))

        for i, (index, label) in enumerate(indices):
            cropped_sentence = np.array(self.sourceFile[str(index)])[0:self.sentence_length]
            inputs_a[i] = np.pad(cropped_sentence, ((self.sentence_length - len(cropped_sentence), 0), (0, 0)),
                                 'constant', constant_values=0)

            if label == LABEL_HUMAN:
                targetFile = self.targetHumanFile
                labelVector = np.array([0, 1])
            else:
                targetFile = self.targetMachineFile
                labelVector = np.array([1, 0])

            cropped_sentence = np.array(targetFile[str(index)])[0:self.sentence_length]
            inputs_b[i] = np.pad(cropped_sentence, ((self.sentence_length - len(cropped_sentence), 0), (0, 0)),
                                 'constant', constant_values=0)

            labels[i] = labelVector

            bar.update(i)

        return inputs_a, inputs_b, labels

    def train(self):
        if self.multilingual:
            model = self.PLSTMModel()
        else:
            model = self.LSTMModel()

        model.compile(optimizer='rmsprop',
                      loss='binary_crossentropy',
                      metrics=['accuracy'])

        
        log_dir=datetime.datetime.now().strftime(f"logs/%d. %B %Y %H-%M, {self.nodes} nodes, {self.dropout} dropout/")
        tensorboard = TensorBoard(log_dir)
        earlyStop = EarlyStopping(monitor='val_acc', mode='max', patience=5, verbose=True, restore_best_weights=True)
        callbacks = [tensorboard, earlyStop, PRTensorBoard(log_dir=log_dir)]

        if self.multilingual:
            model.fit([self.x_train_a, self.x_train_b], self.y_train, batch_size=128, epochs=1000, validation_split=0.1,
                      callbacks=callbacks)
            self.predictions = model.predict([self.x_train_a, self.x_train_b])
        else:
            model.fit(self.x_train_b, self.y_train, batch_size=128, epochs=1000, validation_split=0.1,
                      callbacks=callbacks)
            self.predictions = model.predict(self.x_train_b)
        self.writeResults()

    def writeResults(self):
        predictions = self.predictions

        max_value = 10000
        bar = progressbar.ProgressBar(max_value=max_value * 2, prefix="Writing results")

        with open(f"predictions/{self.nodes} nodes, {self.dropout} dropout.md", "a") as resultsFile, \
                open("source", "r") as sourceFile, \
                open("target.human", "r") as translationHumanFile, \
                open("target.machine", "r") as translationMachineFile:
            resultsFile.write("# Results")

            for i in range(0, max_value * 2, 2):
                humanTranslationIsMachineTranslation, humanTranslationIsHumanTranslation = predictions[i]
                machineTranslationIsMachineTranslation, machineTranslationIsHumanTranslation = predictions[i+1]
                resultsFile.write(f"""
## Sample {int(i/2)}
**Source:** {sourceFile.readline().strip()}

**Human translation:** {translationHumanFile.readline().strip()} 
> prediction: human {humanTranslationIsHumanTranslation:.3f} vs machine {humanTranslationIsMachineTranslation:.3f}

**Machine translation:** {translationMachineFile.readline().strip()} 
> prediction human {machineTranslationIsHumanTranslation:.3f} vs machine {machineTranslationIsMachineTranslation:.3f}
""")

                bar.update(i)



    def PLSTMModel(self):
        input_a = Input(shape=(self.sentence_length, self.word_vector_dim))
        a = LSTM(self.nodes, return_sequences=True)(input_a)
        a = Dropout(self.dropout)(a)
        a = LSTM(self.nodes)(a)
        a = Dropout(self.dropout)(a)
        input_b = Input(shape=(self.sentence_length, self.word_vector_dim))
        b = LSTM(self.nodes, return_sequences=True)(input_b)
        b = Dropout(self.dropout)(b)
        b = LSTM(self.nodes)(b)
        b = Dropout(self.dropout)(b)
        out = concatenate([a, b], axis=-1)
        out = Dense(self.nodes, activation='relu')(out)
        out = Dropout(self.dropout)(out)
        out = Dense(2, activation='softmax')(out)
        model = Model(inputs=[input_a, input_b], outputs=out)
        return model

    def LSTMModel(self):
        input_x = Input(shape=(self.sentence_length, self.word_vector_dim))
        x = LSTM(self.nodes, return_sequences=True)(input_x)
        x = Dropout(self.dropout)(x)
        x = LSTM(self.nodes)(x)
        x = Dropout(self.dropout)(x)
        x = Dense(self.nodes, activation='relu')(x)
        x = Dropout(self.dropout)(x)
        out = Dense(2, activation='softmax')(x)
        model = Model(inputs=[input_x], outputs=out)
        return model


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Analyze vectorized corpora')
    parser.add_argument('--source', default='./source.vectorized.h5', help='Source vector file')
    parser.add_argument('--target-human', type=str, default='./target.human.vectorized.h5',
                        help='Target human vector file.')
    parser.add_argument('--target-machine', type=str, default='./target.machine.vectorized.h5',
                        help='Target machine vector file.')
    parser.add_argument('--multilingual', type=str2bool, default=True,
                        help='Use the multilingual model.')
    parser.add_argument('--dropout', type=float, default=0.5,
                        help='Dropout rate.')
    args = parser.parse_args()

    plstm = PLSTM(args.source, args.target_human, args.target_machine, args.multilingual, args.dropout)

    # nodesOptions = [64, 32, 16, 8]
    nodesOptions = [32]
    # dropoutOptions = [0, 0.3, 0.5]
    dropoutOptions = [0.]

    combinations = [(n, d) for d in dropoutOptions for n in nodesOptions]
    for nodes, dropout in combinations:
        plstm.nodes = nodes
        plstm.dropout = dropout
        plstm.train()
