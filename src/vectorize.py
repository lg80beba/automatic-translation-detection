import argparse
import json

import h5py
import numpy as np

import progressbar
from sty import fg, bg, ef, rs, RgbFg
from collections import Counter, defaultdict
from spacy.lang.en import English
nlp_en = English()

VECTOR_DIM = 300

class Token:
    def __init__(self, token):
        self.token = token
        self.positions = []
        self.vector = None

    def __repr__(self):
        s = ""
        if self.vector is None:
            return fg.red + self.token + fg.rs
        else:
            return fg.green + self.token + fg.rs

    def setVector(self, vector):
        self.vector = vector

    def getJSON(self):
        return json.dumps(self.vector)

    def getNpArray(self):
        return np.asarray(self.vector, dtype=np.float32)

class Vectorizer:
    def __init__(self, args):
        self.textFileName = args.file
        self.vectors_file_name = args.vectors
        self.tokenMap = defaultdict(Token)
        self.textFileLength = self.countLinesOfFile(self.textFileName)
        self.textMap = []

    def run(self):

        self.tokenizeCorpus()

        self.addVectorsToTokens()

        self.removeNotFoundTokens()

        #print(self.textMap)

        self.saveVectorsOfTextMap()

    def saveVectorsOfTextMap(self):
        hf = h5py.File(f"{self.textFileName}.vectorized.h5", 'w')
        for lineNr, line in enumerate(self.textMap):
            lineAsNpArray = np.zeros((len(line), VECTOR_DIM), dtype=np.float32)
            for tokenNr, token in enumerate(line):
                lineAsNpArray[tokenNr] = token.getNpArray()
            hf.create_dataset(str(lineNr), data=lineAsNpArray)

    def addVectorsToTokens(self):
        bar = progressbar.ProgressBar(max_value=self.countLinesOfFile(self.vectors_file_name), prefix="Processing vectors")
        with open(self.vectors_file_name) as vectors:
            for i, vector in enumerate(vectors):
                v_split = vector.split(" ")
                v_token = v_split[0]
                v_vector = v_split[1:]
                if v_token in self.tokenMap:
                    self.tokenMap[v_token].setVector(v_vector)

                bar.update(i)


    def tokenizeCorpus(self):
        bar = progressbar.ProgressBar(max_value=self.countLinesOfFile(self.textFileName), prefix="Reading corpus")
        tokenizer = nlp_en.Defaults.create_tokenizer(nlp_en)

        with open(self.textFileName) as file:
            for lineNumber, line in enumerate(file):
                textMapLine = []

                tokens = tokenizer(line)
                for position, tokenObject in enumerate(tokens):
                    token = str(tokenObject).lower()
                    if token not in self.tokenMap:
                        self.tokenMap[token] = Token(token)

                    textMapLine.append(self.tokenMap[token])
                self.textMap.append(textMapLine)

                bar.update(lineNumber)

    def countLinesOfFile(self, file_path):
        return sum(1 for i in open(file_path, 'rb'))

    def removeNotFoundTokens(self):
        for i, line in enumerate(self.textMap):
            self.textMap[i] = [x for x in line if not x.vector is None]



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='transform sentences into vectors')
    parser.add_argument('file', default='./Corpus/europarl-v7.de-en.en', help='File to vectorize.')
    parser.add_argument('--vectors', type=str, default='./wiki.en.align.vec', help='.vec file.')
    args = parser.parse_args()

    stat = Vectorizer(args)

    stat.run()
