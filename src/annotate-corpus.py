import spacy
import json
import random

nlp_en = spacy.load('en_core_web_sm')
nlp_de = spacy.load('de_core_news_md')

with open('Corpus/de-machine.txt', 'r') as file:
    corpus_machine = [line.strip('\n') for line in file.readlines()]

with open('Corpus/de-human.txt', 'r') as file:
    corpus_human = [line.strip('\n') for line in file.readlines()]

with open('Corpus/en-source.txt', 'r') as file:
    corpus_source = [line.strip('\n') for line in file.readlines()]
    
def annotate(text, nlp):
    keys = ['token','lemma','pos','dep','is_stop']
    values = list(zip(*[(token.text, token.lemma_, token.pos_, token.dep_, token.is_stop) for token in nlp(text)]))
    return dict(zip(keys,values))

corpus = []
for triple in random.sample(list(zip(corpus_machine, corpus_human, corpus_source)), 1_000):
    corpus.append({
            'target_machine': {**{'text': triple[0], 'lang':'de'}, **annotate(triple[0], nlp_de)},
            'target_human': {**{'text': triple[1], 'lang':'de'}, **annotate(triple[1], nlp_de)},
            'source': {**{'text': triple[2], 'lang':'en'}, **annotate(triple[2], nlp_en)} 
        }
    )
    
with open('corpus_1k.json', 'w') as corpus_file:
    json.dump(corpus, corpus_file, indent=4)