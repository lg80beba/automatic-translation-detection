import argparse

import h5py
import matplotlib.pyplot as plt
import numpy as np
import progressbar
import csv


def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def findThreshold(dist1, dist2):
    if np.min(dist1) <= np.max(dist2) and np.max(dist1) >= np.min(dist2):
        return findMiddle(dist1, dist2)

    elif np.min(dist2) <= np.max(dist1) and np.max(dist2) >= np.min(dist1):
        return findMiddle(dist2, dist1)

    elif np.min(dist1) <= np.max(dist2) and np.max(dist1) <= np.min(dist2):
        return np.max(dist1)

    elif np.min(dist2) <= np.max(dist1) and np.max(dist2) <= np.min(dist1):
        return np.max(dist2)
    else:
        return findMiddle(dist1, dist2)

def findMiddle(left, right):
    left.sort()
    right.sort(reverse=True)
    for a, b in zip(left, right):
        if a > b:
            return a

class Statistics:
    def __init__(self, source, target):
        self.sourceFile = source
        self.targetFile = target
        self.length = len(h5py.File(self.sourceFile, 'r'))

    def getSourceTargetPair(self, index):
        source = h5py.File(self.sourceFile, 'r')
        target = h5py.File(self.targetFile, 'r')

        return np.asarray(source[str(index)]), np.asarray(target[str(index)])


    def getAvgCosDist(self):
        bar = progressbar.ProgressBar(max_value=self.length, prefix="Calcualting AvgCosineSim")

        cosSims = []
        for i in range(self.length):
            source, target = self.getSourceTargetPair(i)
            sourceAvg = np.mean(source, axis=0)
            targetAvg = np.mean(target, axis=0)
            if(len(source) > 0):
                cosSims.append(self.cos(sourceAvg, targetAvg))

            bar.update(i)

        print("\nAverage cosine similarity", np.mean(cosSims))
        return cosSims

    def getMinMaxCosSim(self):
        bar = progressbar.ProgressBar(max_value=self.length, prefix="Calcualting MinMaxCosineSim")

        cosSims = []
        for i in range(self.length):
            source, target = self.getSourceTargetPair(i)

            minCos = 1.
            for s in source:
                maxCos = 0.
                for t in target:
                    cos = np.abs(self.cos(s, t))
                    if cos > maxCos:
                        maxCos = cos
                if maxCos < minCos:
                    minCos = maxCos

            if(len(source) > 0):
                cosSims.append(minCos)
            
            bar.update(i)

        print("\nAverage cosine similarity", np.mean(cosSims))
        return cosSims


    def cos(self, a, b):
        return np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Analyze vectorized corpora')
    parser.add_argument('--source', default='./source.txt.vectorized.h5', help='Source vector file')
    parser.add_argument('--target-human', type=str, default='./target.human.txt.vectorized.h5', help='Target human vector file.')
    parser.add_argument('--target-machine', type=str, default='./target.machine.txt.vectorized.h5', help='Target machine vector file.')
    parser.add_argument('--use-avg', type=str2bool, default=True, help='Evaluate average vectors per line, else use MinMax cosine similarity')
    args = parser.parse_args()

    statHuman = Statistics(args.source, args.target_human)
    statMachine = Statistics(args.source, args.target_machine)

    fig, axs = plt.subplots(1, 1, sharey=True, tight_layout=True)

    if args.use_avg:
        human_cos_dist = statHuman.getAvgCosDist()
        machine_cos_dist = statMachine.getAvgCosDist()
        prefix = "average"

    else:
        human_cos_dist = statHuman.getMinMaxCosSim()
        machine_cos_dist = statMachine.getMinMaxCosSim()
        prefix = "minmax"


    with open(f"results_{prefix}.csv", mode='w') as f:
        csv_writer = csv.writer(f, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow([f"{prefix}_human", f"{prefix}_machine"])
        for human, machine in zip(human_cos_dist, machine_cos_dist):
            csv_writer.writerow([human, machine])

    threshold = findThreshold(human_cos_dist, machine_cos_dist)
    print(f"Threshold value: {threshold}")
    print(f"Accuracy value: {sum([1 for i in human_cos_dist if i > threshold])/len(human_cos_dist)}")

    axs.hist((human_cos_dist, machine_cos_dist), density=True, fill=False, bins=400, histtype='step', color=["green", "red"], label=["human", "machine"])
    plt.savefig(f"human(green)-vs-machine(red)-avg.png")
    plt.show()